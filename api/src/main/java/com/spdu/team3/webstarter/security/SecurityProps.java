package com.spdu.team3.webstarter.security;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.Duration;

@ConfigurationProperties("application.security")
public class SecurityProps {
    private String signature;
    private Duration tokenLifeTime;

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public void setTokenLifeTime(Duration tokenLifeTime) {
        this.tokenLifeTime = tokenLifeTime;
    }

    public String getSignature() {
        return signature;
    }

    public Duration getTokenLifeTime() {
        return tokenLifeTime;
    }
}

