package com.spdu.team3.webstarter.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Optional;

import static io.jsonwebtoken.security.Keys.hmacShaKeyFor;
import static java.time.Instant.now;

@Component
public class JwtTokenProvider {
    private static Logger log = LoggerFactory.getLogger(JwtTokenProvider.class);

    private final SecurityProps securityProps;

    public JwtTokenProvider(SecurityProps securityProps) {
        this.securityProps = securityProps;
    }

    Optional<AuthenticatedUser> parseUser(String token) {
        try {
            final Claims claims = Jwts.parser()
                    .setSigningKey(securityProps.getSignature().getBytes())
                    .parseClaimsJws(token)
                    .getBody();

            final AuthenticatedUser user = new AuthenticatedUser(
                    claims.get("id", Integer.class),
                    claims.get("username", String.class),
                    ""
            );

            return Optional.of(user);
        } catch (JwtException e) {
            log.error(e.getMessage(), e);
            return Optional.empty();
        }
    }

    public String createToken(AuthenticatedUser user) {
        return Jwts.builder()
                .setSubject(user.getUsername())
                .claim("id", user.getId())
                .claim("username", user.getUsername())
                .signWith(hmacShaKeyFor(securityProps.getSignature().getBytes()))
                .setExpiration(Date.from(now().plus(securityProps.getTokenLifeTime())))
                .compact();
    }
}

