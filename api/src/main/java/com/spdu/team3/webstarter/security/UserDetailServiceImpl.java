package com.spdu.team3.webstarter.security;

import com.spdu.team3.entity.ApplicationUser;
import com.spdu.team3.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("userDetailsServiceImpl")
public class UserDetailServiceImpl implements UserDetailsService {
    private UserService userService;

    @Autowired
    public UserDetailServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ApplicationUser applicationUser = userService.getByUsername(username);
        if (applicationUser == null) {
            throw new UsernameNotFoundException("User " + username + " not found!");
        }
        return new AuthenticatedUser(applicationUser);
    }
}
