package com.spdu.team3.webstarter.security;

import com.spdu.team3.entity.ApplicationUser;
import org.springframework.security.core.userdetails.User;

import java.util.Collections;

public class AuthenticatedUser extends User {
    private int id;

    public AuthenticatedUser(ApplicationUser applicationUser) {
        this(applicationUser.getId(), applicationUser.getUsername(), applicationUser.getPassword());
    }

    public AuthenticatedUser(int id, String username, String password) {
        super(username, password, Collections.emptyList());
        this.id = id;
    }

    public int getId() {
        return id;
    }
}

