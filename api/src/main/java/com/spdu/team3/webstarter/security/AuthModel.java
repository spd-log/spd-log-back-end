package com.spdu.team3.webstarter.security;

import java.beans.ConstructorProperties;

public class AuthModel {
    private final String username;
    private final String password;

    @ConstructorProperties({"username", "password"})
    public AuthModel(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}

