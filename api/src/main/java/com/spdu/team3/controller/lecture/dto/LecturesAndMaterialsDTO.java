package com.spdu.team3.controller.lecture.dto;

import com.spdu.team3.controller.material.dto.MaterialDTO;

import java.util.List;

public class LecturesAndMaterialsDTO {
    private LectureDTO lectureDTO;
    private List<MaterialDTO> materialDTOs;

    public LectureDTO getLectureDTO() {
        return lectureDTO;
    }

    public void setLectureDTO(LectureDTO lectureDTO) {
        this.lectureDTO = lectureDTO;
    }

    public List<MaterialDTO> getMaterialDTOs() {
        return materialDTOs;
    }

    public void setMaterialDTOs(List<MaterialDTO> materialDTOs) {
        this.materialDTOs = materialDTOs;
    }
}
