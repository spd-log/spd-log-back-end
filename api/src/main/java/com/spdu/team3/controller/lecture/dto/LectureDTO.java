package com.spdu.team3.controller.lecture.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class LectureDTO {
    @NotNull
    private Integer id;

    @NotBlank
    private String name;

    @NotNull
    private CourseDTO courseDTO;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CourseDTO getCourseDTO() {
        return courseDTO;
    }

    public void setCourseDTO(CourseDTO courseDTO) {
        this.courseDTO = courseDTO;
    }
}
