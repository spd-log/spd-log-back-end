package com.spdu.team3.controller.auth;

import com.spdu.team3.controller.auth.dto.AchievementDto;
import com.spdu.team3.controller.auth.dto.ApplicationUserDto;
import com.spdu.team3.controller.auth.dto.ProfileDataDto;
import com.spdu.team3.entity.Achievement;
import com.spdu.team3.entity.ApplicationUser;
import com.spdu.team3.entity.StudentDegree;
import com.spdu.team3.service.user.UserService;
import com.spdu.team3.service.user.UserWithCourseNameAndAchievementBean;
import com.spdu.team3.webstarter.security.AuthModel;
import com.spdu.team3.webstarter.security.AuthenticatedUser;
import com.spdu.team3.webstarter.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/auth")
class AuthController {
    private JwtTokenProvider jwtTokenProvider;
    private AuthenticationManager authenticationManager;
    private UserService userService;

    @Autowired
    public AuthController(JwtTokenProvider jwtTokenProvider,
                          AuthenticationManager authenticationManager,
                          UserService userService) {
        this.jwtTokenProvider = jwtTokenProvider;
        this.authenticationManager = authenticationManager;
        this.userService = userService;
    }

    @PostMapping("/login")
    public ResponseEntity<ProfileDataDto> login(@RequestBody AuthModel authModel) {
        final var authenticationToken = new UsernamePasswordAuthenticationToken(authModel.getUsername(), authModel.getPassword());
        final AuthenticatedUser principal = (AuthenticatedUser) authenticationManager.authenticate(authenticationToken).getPrincipal();
        final String token = jwtTokenProvider.createToken(principal);

        UserWithCourseNameAndAchievementBean applicationUser = userService.getUserWithCourseNameAndAchieve(principal.getUsername());

        ProfileDataDto profileDataDto = getDataForProfile(applicationUser);
        profileDataDto.setToken(new AuthTockenModel(token));

        return new ResponseEntity<>(profileDataDto, HttpStatus.OK);
    }

    private ProfileDataDto getDataForProfile(UserWithCourseNameAndAchievementBean applicationUser) {
        ProfileDataDto profileDataDto = new ProfileDataDto();
        profileDataDto.setApplicationUserDto(toDtoUser(applicationUser.getApplicationUser()));
        profileDataDto.setCourseName(applicationUser.getCourseName());
        profileDataDto.setLevelOfProfile(applicationUser.getLevel());
        if (applicationUser.getAchievementsForBar().size() > 0) {
            profileDataDto.setAchievements(toAchievementsDto(applicationUser.getAchievementsForBar()));
        }

        return profileDataDto;
    }

    private List<AchievementDto> toAchievementsDto(List<Achievement> achievementsForBar) {
        List<AchievementDto> achievements = new ArrayList<>();
        for (Achievement achievement : achievementsForBar) {
            AchievementDto achievementDto = new AchievementDto();
            achievementDto.setName(achievement.getName());
            achievementDto.setImageUrl(achievement.getImageUrl());

            achievements.add(achievementDto);
        }

        return achievements;
    }

    private ApplicationUserDto toDtoUser(ApplicationUser applicationUser) {
        ApplicationUserDto applicationUserDto = new ApplicationUserDto();
        applicationUserDto.setId(applicationUser.getId());
        applicationUserDto.setUsername(applicationUser.getUsername());
        applicationUserDto.setFirstName(applicationUser.getFirstName());
        applicationUserDto.setLastName(applicationUser.getLastName());
        applicationUserDto.setPhoto(applicationUser.getPhoto());

        return applicationUserDto;
    }
}