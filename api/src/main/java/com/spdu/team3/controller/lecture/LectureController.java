package com.spdu.team3.controller.lecture;

import com.spdu.team3.controller.lecture.dto.CourseDTO;
import com.spdu.team3.controller.lecture.dto.LectureDTO;
import com.spdu.team3.controller.lecture.dto.LecturesAndMaterialsDTO;
import com.spdu.team3.controller.material.dto.MaterialDTO;
import com.spdu.team3.entity.Lecture;
import com.spdu.team3.entity.Material;
import com.spdu.team3.service.LectureService;
import com.spdu.team3.service.MaterialService;
import com.spdu.team3.webstarter.security.AuthenticatedUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/lectures")
public class LectureController {
    private LectureService lectureService;
    private MaterialService materialService;

    @Autowired
    public LectureController(LectureService lectureService,
                             MaterialService materialService) {
        this.lectureService = lectureService;
        this.materialService = materialService;
    }

    @GetMapping
    public ResponseEntity<List<LectureDTO>> getAllLectures(@AuthenticationPrincipal AuthenticatedUser user) {
        List<Lecture> lectures = lectureService.getAllLecturesForUser(user.getId());

        List<LectureDTO> lectureDTOs = new ArrayList<>();

        for (Lecture lecture : lectures) {
            lectureDTOs.add(mapEntityToDTO(lecture));
        }

        return new ResponseEntity<>(lectureDTOs, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<LecturesAndMaterialsDTO> getLecture(@PathVariable int id) {
        Lecture lecture = lectureService.getLecture(id);

        LecturesAndMaterialsDTO lecturesAndMaterialsDTO = new LecturesAndMaterialsDTO();

        LectureDTO lectureDTO = mapEntityToDTO(lecture);

        List<Material> materials = materialService.getAllMaterial(id);

        List<MaterialDTO> materialDTOs = mapMaterialEntityToDTO(materials);

        lecturesAndMaterialsDTO.setLectureDTO(lectureDTO);
        lecturesAndMaterialsDTO.setMaterialDTOs(materialDTOs);

        return new ResponseEntity<>(lecturesAndMaterialsDTO, HttpStatus.OK);
    }

    private List<MaterialDTO> mapMaterialEntityToDTO(List<Material> materials) {
        List<MaterialDTO> materialDTOs = new ArrayList<>();

        for (Material material : materials) {
            MaterialDTO materialDTO = new MaterialDTO();

            materialDTO.setId(material.getId());
            materialDTO.setName(material.getName());
            materialDTO.setUrlLink(material.getUrlLink());
            materialDTO.setType(material.getType());

            LectureDTO lectureDTO = new LectureDTO();

            lectureDTO.setId(material.getLecture().getId());
            lectureDTO.setName(material.getLecture().getName());

            CourseDTO courseDTO = new CourseDTO();

            courseDTO.setId(material.getLecture().getCourse().getId());
            courseDTO.setName(material.getLecture().getCourse().getName());

            lectureDTO.setCourseDTO(courseDTO);

            materialDTO.setLectureDTO(lectureDTO);

            materialDTOs.add(materialDTO);
        }

        return materialDTOs;
    }

    private LectureDTO mapEntityToDTO(Lecture lecture) {
        LectureDTO lectureDTO = new LectureDTO();

        lectureDTO.setId(lecture.getId());
        lectureDTO.setName(lecture.getName());

        CourseDTO courseDTO = new CourseDTO();

        courseDTO.setId(lecture.getCourse().getId());
        courseDTO.setName(lecture.getCourse().getName());

        lectureDTO.setCourseDTO(courseDTO);

        return lectureDTO;
    }
}
