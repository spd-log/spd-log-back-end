package com.spdu.team3.controller.auth.dto;

import com.spdu.team3.controller.auth.AuthTockenModel;

import java.util.List;

public class ProfileDataDto {
    private ApplicationUserDto applicationUserDto;
    private String courseName;
    private int levelOfProfile;
    private List<AchievementDto> achievements;
    private AuthTockenModel token;

    public ApplicationUserDto getApplicationUserDto() {
        return applicationUserDto;
    }

    public void setApplicationUserDto(ApplicationUserDto applicationUserDto) {
        this.applicationUserDto = applicationUserDto;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getLevelOfProfile() {
        return levelOfProfile;
    }

    public void setLevelOfProfile(int levelOfProfile) {
        this.levelOfProfile = levelOfProfile;
    }

    public List<AchievementDto> getAchievements() {
        return achievements;
    }

    public void setAchievements(List<AchievementDto> achievements) {
        this.achievements = achievements;
    }

    public AuthTockenModel getToken() {
        return token;
    }

    public void setToken(AuthTockenModel token) {
        this.token = token;
    }
}
