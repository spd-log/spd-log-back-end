package com.spdu.team3.controller.achievement;

import com.spdu.team3.controller.achievement.dto.AchievementDTO;
import com.spdu.team3.controller.lecture.dto.CourseDTO;
import com.spdu.team3.entity.Achievement;
import com.spdu.team3.service.AchievementService;
import com.spdu.team3.webstarter.security.AuthenticatedUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/achievements")
public class AchievementController {
    private AchievementService achievementService;

    @Autowired
    public AchievementController(AchievementService achievementService) {
        this.achievementService = achievementService;
    }

    @GetMapping
    public ResponseEntity<List<AchievementDTO>> getAllAchievement(@AuthenticationPrincipal AuthenticatedUser user) {
        List<Achievement> achievements = achievementService.getAllAchievement(user.getId());

        List<AchievementDTO> achievementDTOs = mapEntitiesToDTOs(achievements);

        return new ResponseEntity<>(achievementDTOs, HttpStatus.OK);
    }

    private List<AchievementDTO> mapEntitiesToDTOs(List<Achievement> achievements) {
        List<AchievementDTO> achievementDTOs = new ArrayList<>();

        for (Achievement achievement : achievements) {
            AchievementDTO achievementDTO = new AchievementDTO();

            achievementDTO.setId(achievement.getId());
            achievementDTO.setName(achievement.getName());
            achievementDTO.setImageUrl(achievement.getImageUrl());

            CourseDTO courseDTO = new CourseDTO();

            courseDTO.setId(achievement.getCourse().getId());
            courseDTO.setName(achievement.getCourse().getName());

            achievementDTO.setCourseDTO(courseDTO);

            achievementDTOs.add(achievementDTO);
        }

        return achievementDTOs;
    }
}
