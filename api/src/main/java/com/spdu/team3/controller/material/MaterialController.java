package com.spdu.team3.controller.material;

import com.spdu.team3.entity.Material;
import com.spdu.team3.service.MaterialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/materials")
public class MaterialController {
    private MaterialService materialService;

    @Autowired
    public MaterialController(MaterialService materialService) {
        this.materialService = materialService;
    }

    @GetMapping
    public ResponseEntity<List<Material>> getAllMaterials(@RequestParam(required = true) int lectureId) {
        List<Material> materials = materialService.getAllMaterial(lectureId);

        return new ResponseEntity<>(materials, HttpStatus.OK);
    }
}
