package com.spdu.team3.controller.auth;

public class AuthTockenModel {
    private String token;

    public AuthTockenModel(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}

