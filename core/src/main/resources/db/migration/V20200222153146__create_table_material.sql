CREATE TABLE material
(
    id SERIAL PRIMARY KEY NOT NULL,
    name VARCHAR (200) NOT NULL,
    url_link VARCHAR (300) NOT NULL,
    lecture_id INTEGER REFERENCES lecture (id) NOT NULL,
    type VARCHAR (30) NOT NULL
);