CREATE TABLE lecture
(
    id SERIAL PRIMARY KEY NOT NULL,
    name VARCHAR (200) NOT NULL,
    course_id INTEGER REFERENCES course (id)
);