INSERT INTO application_user (username, password, first_name, last_name, photo)
VALUES
	('D.Kondayrov',
	'$2a$10$Bf00TGyyJzYo8jANbxU3ZexoQoGi.dj7NUtgQ3uQJR5hEh4RzUM2.',
	'Дмитро','Кондауров',
	'https://www.meme-arsenal.com/memes/5a61d9c84179866740b3fcacd744b95d.jpg'),--001
	('V.Muzychenko',
	'$2a$10$qSSLk9O5DxtApAgksTvJ/epOomy4O6D/JGUzXXhUeNbvGz8uDSE26',
	'Влад','Музиченко',
	'https://4tololo.ru/files/styles/large/public/images/20182306142656.jpg'),--002
	('O.Osadchyi',
	'$2a$10$hKdx1z.DhZzg6s.VzQuRI.Xrzl060OwZ2yTUuB80OhlgvbelMXsmO',
	'Олексій','Осадчий',
	'https://www.vladtime.ru/uploads/posts/2018-03/1520229914_kartinki24_ru_men_80.jpg'),--003
	('A.Loboda',
	'$2a$10$4phyqnPKKn24N6zk0hJgyuhDJezfkNX4OjOoM2mS2LeOIT8mffwI2',
	'Андрій','Лобода',
	'https://fitneson.ru/wp-content/uploads/2019/05/russkie-aktery-kachki-kurtsyn.jpg'),--004
	('V.Holovko',
	'$2a$10$tdI7joMC5CsdAWHeG/iKcOd0jTNg3vrrqAPCtLpwCQbKe9WLe3ipq','Влад','Головко',
	'https://tutknow.ru/uploads/posts/2017-05/1494115037_pochemu-kachki-puzatye-prichiny.jpg');--005