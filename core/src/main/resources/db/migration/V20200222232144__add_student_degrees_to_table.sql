INSERT INTO student_degree (user_id, course_id, active)
VALUES
    (1, 2, TRUE),
    (2, 1, TRUE),
    (2, 3, FALSE),
    (3, 1, TRUE),
    (4, 3, TRUE),
    (5, 2, TRUE);