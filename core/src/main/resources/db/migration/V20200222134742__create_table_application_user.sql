CREATE TABLE application_user
(
    id SERIAL PRIMARY KEY NOT NULL,
    username VARCHAR(200) NOT NULL,
	password VARCHAR(200) NOT NULL,
	first_name VARCHAR(200) NOT NULL,
	last_name VARCHAR(200) NOT NULL,
	photo VARCHAR (200) NOT NULL
);