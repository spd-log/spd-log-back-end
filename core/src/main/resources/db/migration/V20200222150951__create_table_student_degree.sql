CREATE TABLE student_degree
(
    id SERIAL PRIMARY KEY NOT NULL,
    user_id INTEGER REFERENCES application_user (id) NOT NULL,
    course_id INTEGER REFERENCES course (id) NOT NULL,
    active BOOLEAN NOT NULL
);