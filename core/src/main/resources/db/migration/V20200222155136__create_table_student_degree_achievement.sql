CREATE TABLE student_degree_achievement
(
    student_degree_id INTEGER REFERENCES student_degree (id) NOT NULL,
    achievement_id INTEGER REFERENCES achievement (id) NOT NULL,
    PRIMARY KEY (student_degree_id, achievement_id)
);