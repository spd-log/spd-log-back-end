CREATE TABLE achievement
(
    id SERIAL PRIMARY KEY NOT NULL,
    name VARCHAR (50) NOT NULL,
    image_url VARCHAR (200) NOT NULL,
    course_id INTEGER REFERENCES course (id) NOT NULL
);