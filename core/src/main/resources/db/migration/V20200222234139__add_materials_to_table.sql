INSERT INTO material (name, url_link, lecture_id, type)
VALUES
    ('Creating and Starting Java Threads', 'http://tutorials.jenkov.com/java-concurrency/creating-and-starting-threads.html', 1, 'ARTICLE'),
    ('Java Threads Tutorial 1 - Introduction to Java Threads', 'https://www.youtube.com/watch?v=b5sj13Z7aho', 1, 'VIDEO'),
    ('Built-in Functional Interfaces', 'https://drive.google.com/file/d/1SODDa0MMGCjQoEDLGF8LsFUhHKjEavxB/view', 1, 'PRESENTATION'),
    ('Что такое Virtual DOM?', 'https://habr.com/ru/post/256965/', 3, 'ARTICLE'),
    ('86 - Virtual DOM - React JS', 'https://www.youtube.com/watch?v=rsW9_UtF4jk', 3, 'VIDEO'),
    ('Virtual dom', 'https://www.slideshare.net/gyeongseokseo/virtual-dom', 3, 'PRESENTATION'),
    ('nodemon', 'https://www.npmjs.com/package/nodemon', 4, 'ARTICLE'),
    ('Node JS Tutorial for Beginners #22 - Installing Nodemon', 'https://www.youtube.com/watch?v=4N0d8HhU5DE', 4, 'VIDEO');
