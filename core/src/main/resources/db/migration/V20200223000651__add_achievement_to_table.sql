INSERT INTO achievement (name, image_url, course_id)
VALUES
('Завершив курс по Java Core', 'https://upload.wikimedia.org/wikipedia/uk/thumb/8/85/%D0%9B%D0%BE%D0%B3%D0%BE%D1%82%D0%B8%D0%BF_Java.png/250px-%D0%9B%D0%BE%D0%B3%D0%BE%D1%82%D0%B8%D0%BF_Java.png', 1),
('Оволодів  можливостями Spring-boot', 'https://avatars.mds.yandex.net/get-zen_doc/1131857/pub_5ac33f542f578c01b35b02b9_5ac3413ad7bf2113e95dcf40/scale_1200', 1),
('Завершив 5 домашніх завдань без затримок', 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/TV5_%28ABC5%29_Logo.svg/1200px-TV5_%28ABC5%29_Logo.svg.png', 1),
('Завершив 5 домашніх завдань без затримок', 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/TV5_%28ABC5%29_Logo.svg/1200px-TV5_%28ABC5%29_Logo.svg.png', 2),
('Завершив 5 домашніх завдань без затримок', 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/TV5_%28ABC5%29_Logo.svg/1200px-TV5_%28ABC5%29_Logo.svg.png', 3),
('Завершив курс по React', 'https://skyup.studio/wp-content/uploads/2019/02/react-logo-300x289.png', 3),
('Повністю пройшов курс Java for web', 'https://static6.depositphotos.com/1025340/566/i/450/depositphotos_5660952-stock-photo-laurel-wreath.jpg', 1),
('Повністю пройшов курс Front-end', 'https://static6.depositphotos.com/1025340/566/i/450/depositphotos_5660952-stock-photo-laurel-wreath.jpg', 2),
('Повністю пройшов курс Java for web', 'https://static6.depositphotos.com/1025340/566/i/450/depositphotos_5660952-stock-photo-laurel-wreath.jpg', 1),
('Повністю пройшов курс Node js', 'https://static6.depositphotos.com/1025340/566/i/450/depositphotos_5660952-stock-photo-laurel-wreath.jpg', 3),
('Оволодів технологією Thymeleaf', 'https://pbs.twimg.com/profile_images/714595968612696064/1ll27Cbc_400x400.jpg', 1),
('Оволодів навичками TypeScript', 'https://miro.medium.com/max/816/1*mn6bOs7s6Qbao15PMNRyOA.png', 3),
('Оволодів навичками HTML', 'http://w3.org.ua/wp-content/uploads/2017/01/HTML5.jpg', 3),
('Оволодів навичками CSS', 'https://vignette.wikia.nocookie.net/wikies/images/a/a9/CSS3.png/revision/latest/scale-to-width-down/340?cb=20160909123652&path-prefix=ru', 3),
('Оволодів технологією JDBC', 'https://razilov-code.ru/wp-content/uploads/2018/07/java-jdbc.png', 1);
