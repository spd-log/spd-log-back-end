INSERT INTO lecture (name, course_id)
VALUES
    ('Threads', 1),
    ('Spring Framework', 1),
    ('Virtual DOM', 3),
    ('Nodemon', 2),
    ('React introduce', 3),
    ('Context, Exception Handing', 3),
    ('Typescript', 3),
    ('HTML introduce', 3),
    ('CSS introduce', 3);