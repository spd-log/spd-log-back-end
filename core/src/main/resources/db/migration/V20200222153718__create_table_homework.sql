CREATE TABLE homework
(
    id SERIAL PRIMARY KEY NOT NULL,
    name VARCHAR (50) NOT NULL,
    description VARCHAR (5000) NOT NULL,
    lecture_id INTEGER REFERENCES lecture (id) NOT NULL
);