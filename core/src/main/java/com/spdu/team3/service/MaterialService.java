package com.spdu.team3.service;

import com.spdu.team3.entity.Material;
import com.spdu.team3.repository.MaterialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MaterialService {
    private MaterialRepository materialRepository;

    @Autowired
    public MaterialService(MaterialRepository materialRepository) {
        this.materialRepository = materialRepository;
    }

    public List<Material> getAllMaterial(int lectureId) {
        return materialRepository.findAllByLectureIdAndType(lectureId);
    }
}
