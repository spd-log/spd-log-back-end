package com.spdu.team3.service.user;

import com.spdu.team3.entity.Achievement;
import com.spdu.team3.entity.ApplicationUser;
import com.spdu.team3.entity.Course;

import java.util.List;

public class UserWithCourseNameAndAchievementBean {
    private ApplicationUser applicationUser;
    private String courseName;
    private int level;
    private List<Achievement> achievementsForBar;

    public UserWithCourseNameAndAchievementBean(ApplicationUser applicationUser, String courseName, int level, List<Achievement> achievementsForBar) {
        this.applicationUser = applicationUser;
        this.courseName = courseName;
        this.level = level;
        this.achievementsForBar = achievementsForBar;
    }

    public ApplicationUser getApplicationUser() {
        return applicationUser;
    }

    public void setApplicationUser(ApplicationUser applicationUser) {
        this.applicationUser = applicationUser;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public List<Achievement> getAchievementsForBar() {
        return achievementsForBar;
    }

    public void setAchievementsForBar(List<Achievement> achievementsForBar) {
        this.achievementsForBar = achievementsForBar;
    }
}
