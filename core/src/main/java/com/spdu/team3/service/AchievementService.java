package com.spdu.team3.service;

import com.spdu.team3.entity.Achievement;
import com.spdu.team3.repository.AchievementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AchievementService {
    private AchievementRepository achievementRepository;

    @Autowired
    public AchievementService(AchievementRepository achievementRepository) {
        this.achievementRepository = achievementRepository;
    }

    public List<Achievement> getAllAchievement(int userId) {
        return achievementRepository.findAllAchievementByUserId(userId);
    }
}
