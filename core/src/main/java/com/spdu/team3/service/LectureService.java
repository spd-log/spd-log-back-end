package com.spdu.team3.service;

import com.spdu.team3.entity.Lecture;
import com.spdu.team3.exception.NotFoundException;
import com.spdu.team3.repository.LectureRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LectureService {

    private LectureRepository lectureRepository;

    public LectureService(LectureRepository lectureRepository) {
        this.lectureRepository = lectureRepository;
    }

    public Lecture getLecture(int id) {
        return lectureRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Лекції з таким id - не існує!"));
    }

    public List<Lecture> getAllLecturesForUser(int userId) {
        return lectureRepository.findAllByUserId(userId);
    }
}
