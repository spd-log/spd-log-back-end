package com.spdu.team3.service.user;

import com.spdu.team3.entity.Achievement;
import com.spdu.team3.entity.ApplicationUser;
import com.spdu.team3.entity.StudentDegree;
import com.spdu.team3.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class UserService {
    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public ApplicationUser getByUsername(String username) {
            return userRepository.findByUsername(username);
    }

    public UserWithCourseNameAndAchievementBean getUserWithCourseNameAndAchieve(String username) {
        ApplicationUser applicationUser = getByUsername(username);
        UserWithCourseNameAndAchievementBean user;
        String courseName = "";
        int levelOfProfile = 0;
        List<Achievement> firstThreeAchieve = new ArrayList<>();

        if (applicationUser != null) {
            Set<StudentDegree> degrees = applicationUser.getStudentDegrees();
            if (degrees.size() == 1) {
                for (StudentDegree degree : degrees) {
                    if (degree.isActive()) {
                        courseName = degree.getCourse().getName();

                        List<Achievement> achievements = degree.getAchievements();
                        if (achievements.size() > 0) {
                            levelOfProfile = achievements.size();
                        }
                        if (achievements.size() > 3) {
                            firstThreeAchieve.add(achievements.get(0));
                            firstThreeAchieve.add(achievements.get(1));
                            firstThreeAchieve.add(achievements.get(2));
                        } else {
                            firstThreeAchieve.addAll(achievements);
                        }
                    }
                }
            }
        }

        return new UserWithCourseNameAndAchievementBean(applicationUser, courseName, levelOfProfile, firstThreeAchieve);
    }
}
