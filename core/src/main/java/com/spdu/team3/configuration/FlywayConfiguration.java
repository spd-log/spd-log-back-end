package com.spdu.team3.configuration;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application-psql.properties")
public class FlywayConfiguration {
    @Bean
    public Flyway migrationStrategy(
            @Value("${datasource.url}") String url,
            @Value("${datasource.username}") String userName,
            @Value("${datasource.password}") String password
    ) {
        Flyway flyway = Flyway
                .configure()
                .dataSource(
                        url,
                        userName,
                        password
                )
                .load();
        flyway.migrate();

        return flyway;
    }
}
