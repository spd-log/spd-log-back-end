package com.spdu.team3.repository;

import com.spdu.team3.entity.Lecture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface LectureRepository extends JpaRepository<Lecture, Integer> {
    @Query(value = "SELECT * FROM lecture as l " +
            "INNER JOIN course as c ON l.course_id = c.id " +
            "INNER JOIN student_degree as sd ON sd.course_id = c.id " +
            "INNER JOIN application_user as au ON sd.user_id = au.id " +
            "WHERE au.id = :userId AND sd.active = true", nativeQuery = true)
    List<Lecture> findAllByUserId(@Param("userId") int userId);
}
