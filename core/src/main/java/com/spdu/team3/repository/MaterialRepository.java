package com.spdu.team3.repository;

import com.spdu.team3.entity.Lecture;
import com.spdu.team3.entity.Material;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MaterialRepository extends JpaRepository<Material, Integer> {
    @Query(value = "SELECT * FROM material as m " +
            "INNER JOIN lecture as l ON m.lecture_id = l.id " +
            "WHERE l.id = :lectureId", nativeQuery = true)
    List<Material> findAllByLectureIdAndType(@Param("lectureId") int lectureId);
}
