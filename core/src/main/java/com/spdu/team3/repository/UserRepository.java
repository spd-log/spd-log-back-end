package com.spdu.team3.repository;

import com.spdu.team3.entity.ApplicationUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface UserRepository extends JpaRepository<ApplicationUser, Integer> {
    @Query("select au from ApplicationUser au " +
            "where lower(au.username) = lower(:username)" +
            "and  lower(au.password) = lower(:password)")
    ApplicationUser findByUsernameAndPassword(@Param("username") String username,
                                              @Param("password") String password
    );

    @Transactional
    ApplicationUser findByUsername(String username);
}
