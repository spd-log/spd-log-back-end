package com.spdu.team3.repository;

import com.spdu.team3.entity.Achievement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AchievementRepository  extends JpaRepository<Achievement, Integer> {
    @Query(value = "SELECT * FROM achievement as a " +
            "INNER JOIN student_degree_achievement as sda ON sda.achievement_id = a.id " +
            "INNER JOIN student_degree as sd ON sd.id = sda.student_degree_id " +
            "INNER JOIN application_user as au ON sd.user_id = au.id " +
            "WHERE au.id = :userId AND sd.active = TRUE", nativeQuery = true)
    List<Achievement> findAllAchievementByUserId(@Param("userId") int userId);
}
