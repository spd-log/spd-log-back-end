package com.spdu.team3.entity;

import com.spdu.team3.entity.superclasses.BaseEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class StudentDegree extends BaseEntity {
    @ManyToOne
    private ApplicationUser user;

    @ManyToOne
    private Course course;

    @Column
    private boolean active;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "student_degree_achievement",
        joinColumns = @JoinColumn(name = "student_degree_id"),
        inverseJoinColumns = @JoinColumn(name = "achievement_id")
    )
    private List<Achievement> achievements = new ArrayList<>();

    public ApplicationUser getUser() {
        return user;
    }

    public void setUser(ApplicationUser applicationUser) {
        this.user = user;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<Achievement> getAchievements() {
        return achievements;
    }

    public void setAchievements(List<Achievement> achievements) {
        this.achievements = achievements;
    }
}
