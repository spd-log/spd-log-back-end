package com.spdu.team3.entity;

import com.spdu.team3.entity.superclasses.NameEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Achievement extends NameEntity {
    @ManyToOne
    private Course course;

    @Column
    private String imageUrl;

    @ManyToMany(mappedBy = "achievements")
    private List<StudentDegree> studentDegrees = new ArrayList<>();

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<StudentDegree> getStudentDegrees() {
        return studentDegrees;
    }

    public void setStudentDegrees(List<StudentDegree> studentDegrees) {
        this.studentDegrees = studentDegrees;
    }
}
