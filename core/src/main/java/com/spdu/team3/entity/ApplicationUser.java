package com.spdu.team3.entity;

import com.spdu.team3.entity.superclasses.Person;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class ApplicationUser extends Person {
    @Column
    private String username;
    @Column
    private String password;
    @Column
    private String photo;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<StudentDegree> studentDegrees = new HashSet<>();

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<StudentDegree> getStudentDegrees() {
        return studentDegrees;
    }

    public void setStudentDegrees(Set<StudentDegree> studentDegrees) {
        this.studentDegrees = studentDegrees;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}