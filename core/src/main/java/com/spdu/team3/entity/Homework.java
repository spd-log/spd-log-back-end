package com.spdu.team3.entity;

import com.spdu.team3.entity.superclasses.NameEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Homework extends NameEntity {
    @Column
    private String description;

    @ManyToOne
    private Lecture lecture;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Lecture getLecture() {
        return lecture;
    }

    public void setLecture(Lecture lecture) {
        this.lecture = lecture;
    }
}
